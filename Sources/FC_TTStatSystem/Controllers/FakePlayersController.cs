﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FC_TTCore.Models;
using FC_TTStatSystem.Services;


namespace FC_TTStatSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FakePlayersController : ControllerBase
    {
        private FakeDbContext _context;

        public FakePlayersController(FakeDbContextService contextService)
        {
            _context = contextService.DBContext;
        }

        // GET: api/FakePlayers
        [HttpGet]
        public IEnumerable<FakePlayer> Get()
        {
            return _context.FakePlayers;
        }

        // GET: api/FakePlayers/1
        [HttpGet("{id}", Name = "GetFakePlayer")]
        public FakePlayer Get(int id)
        {
            return _context.FakePlayers[id];
        }
    }
}