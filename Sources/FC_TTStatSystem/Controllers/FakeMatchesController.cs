﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FC_TTCore.Models;
using FC_TTStatSystem.Services;

namespace FC_TTStatSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FakeMatchesController : ControllerBase
    {
        private FakeDbContext _context;

        public FakeMatchesController(FakeDbContextService contextService)
        {
            _context = contextService.DBContext;
        }

        // GET: api/FakePlayers
        [HttpGet]
        public IEnumerable<FakeMatch> Get()
        {
            return _context.FakeMatches;
        }

        // GET: api/FakePlayers/1
        [HttpGet("{id}", Name = "GetFakeMatch")]
        public FakeMatch Get(int id)
        {
            return _context.FakeMatches[id];
        }
    }
}